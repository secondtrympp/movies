package repository;

import domain.Movie;
import domain.exceptions.MovieRentalException;
import domain.validators.Validator;
import pagination.Page;
import pagination.PageGenerator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DBMovieRepo implements PagingRepository<Integer, Movie> {
    //Supplier<String>
    //Function<ResultSet, List<T>>
    //Function<T, String>
    //Function<T, String>
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "12345678";
    private static final String URL = "jdbc:postgresql://localhost:5432/movierental";
    private Validator<Movie> movieValidator;

    public DBMovieRepo(Validator<Movie> movieValidator){
        this.movieValidator = movieValidator;
    }


    public Iterable<Movie> findAll(){

        List<Movie> movies = new ArrayList<>();
        String cmd = "select * from Movie";
        try(
                Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                PreparedStatement statement = conn.prepareStatement(cmd);
                ResultSet resultSet = statement.executeQuery();
                )
        {
                while(resultSet.next()){
                    int ID = resultSet.getInt("ID");
                    String title = resultSet.getString("title");
                    String genre = resultSet.getString("genre");
                    float rating = resultSet.getFloat("rating");
                    movies.add(new Movie(ID, title,genre, rating));
                }
        }
        catch (SQLException sqlx){
            sqlx.printStackTrace();
            throw new MovieRentalException("Cannot find movies");
        }
        return movies;
    }
    public Optional<Movie> save(Movie entity){
        String cmd = "insert into Movie(ID, title, genre, rating) values (?,?,?,?)";
        movieValidator.validate(entity);
        try(
                Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                PreparedStatement statement = conn.prepareStatement(cmd);

        ) {
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getTitle());
            statement.setString(3,entity.getGenre());
            statement.setFloat(4, entity.getRating());
           int res = statement.executeUpdate();
           if(res == 1){
               return Optional.empty();
           }
           return Optional.ofNullable(entity);
        }
        catch(SQLException sqlx){
            throw new MovieRentalException("Database inconsistent");
        }
    }
    @Override
    public Optional<Movie> update(Movie entity){
        String cmd = "update Movie set title=?, genre=?, rating=? where ID=?";
        movieValidator.validate(entity);
        try(
                Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                PreparedStatement statement = conn.prepareStatement(cmd);

        ) {
            statement.setInt(4, entity.getId());
            statement.setString(1, entity.getTitle());
            statement.setString(2,entity.getGenre());
            statement.setFloat(3, entity.getRating());
            int res = statement.executeUpdate();
            if(res == 1){
                return Optional.empty();
            }
            return Optional.ofNullable(entity);
        }
        catch(SQLException sqlx){
            throw new MovieRentalException("Database inconsistency while update");
        }

    }
    @Override
    public Optional<Movie> delete(Integer id){
        String cmd = "delete from Movie where ID=" +id;
        try(
                Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                PreparedStatement statement = conn.prepareStatement(cmd);
        ) {
            int res = statement.executeUpdate();
            if (res == 1)
                return Optional.ofNullable(new Movie(1,"","", 0));
            return Optional.empty();
        }
        catch(SQLException sqlx){
                throw new MovieRentalException("Database inconsistency while update");
        }
    }
    @Override
    public Optional<Movie> findOne(Integer id) {
        String cmd = "select from Movie where ID=" + id;
        try (
                Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                PreparedStatement statement = conn.prepareStatement(cmd);
        ) {
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                int ID = resultSet.getInt("ID");
                String title = resultSet.getString("title");
                String genre = resultSet.getString("genre");
                float rating = resultSet.getFloat("rating");
                return Optional.ofNullable(new Movie(ID, title,genre, rating));
            }
            return Optional.empty();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new MovieRentalException("DB connection inconsistent");
        }
    }
    @Override
    public Page<Movie> findAll(PageGenerator pageGenerator){
        int baseL = pageGenerator.getCurrentPage()*pageGenerator.getPageSize();
        PageGenerator next = new PageGenerator(pageGenerator.getCurrentPage() + 1, pageGenerator.getPageSize());
        List<Movie> movies = StreamSupport.stream(findAll().spliterator(), false).skip(baseL).limit(next.getPageSize()).collect(Collectors.toList());
        return new Page<>(movies, next);
    }
}
