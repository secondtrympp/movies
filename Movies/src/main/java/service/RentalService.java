package service;

import domain.Client;
import domain.Movie;
import domain.Rental;
import domain.exceptions.ClientNotFound;
import domain.exceptions.MovieNotFound;
import domain.exceptions.MovieRentalException;
import pagination.Page;
import pagination.PageGenerator;
import repository.PagingRepository;
import repository.Repository;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class RentalService implements Service<Integer,Rental> {

    private Repository<Integer,Rental> rentalRepository;
    private Repository<Integer,Movie> movieRepository;
    private Repository<Long,Client> clientRepository;
    private static AtomicInteger ID=new AtomicInteger(0);
    private PageGenerator pageGenerator = new PageGenerator();

    /**
     * Constructor
     * @param rRepo, rental repository
     * @param mRepo, movie repository
     * @param cRepo, client repository
     */
    public RentalService(Repository rRepo,Repository mRepo,Repository cRepo){
        rentalRepository=rRepo;
        movieRepository=mRepo;
        clientRepository=cRepo;
        initID();
    }

    /**
     * Saves a new rental to the repository
     * @param cId, the ID of the client who rented the movie
     * @param mId, the ID of the movie rented
     * @param startDate, the start date of the rental
     * @param days, the number of days rented for
     * @return and {@code Optional} which is empty if the rental was added
     * or contains the already existing rental
     */
    public Optional<Rental> save(long cId, int mId, LocalDate startDate,int days){
        LocalDate endDate=startDate.plusDays(days);
        Optional<Client> client=clientRepository.findOne(cId);
        client.orElseThrow(()->new ClientNotFound("Client does not exist!"));
        Optional<Movie> movie=movieRepository.findOne(mId);
        movie.orElseThrow(()->new MovieNotFound("Movie does not exist!"));
        return rentalRepository.save(new Rental(ID.incrementAndGet(),
                    cId,mId,startDate,endDate));
    }

    /**
     * Provides all rentals
     * @return a {@code Set} containing all rentals in the repository
     */
    public Set<Rental> getAllRentals(){
        return StreamSupport.stream(rentalRepository.findAll().spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Provides all rentals of a given client
     * @param cId, the ID of the client
     * @return a {@code Set} containing the rentals for the given client
     */
    public Set<Rental> getRentalsByClient(long cId){
        return StreamSupport.stream(rentalRepository.findAll().spliterator(), false).filter(r->r.getClientId()==cId).collect(Collectors.toSet());
    }

    /**
     * Provides all rentals of a given client
     * @param mId, the ID of the movie
     * @return a {@code Set} containing the rentals for the given movie
     */
    public Set<Rental> getRentalByMovie(int mId){
        return StreamSupport.stream(rentalRepository.findAll().spliterator(), false).filter(r->r.getMovieId()==mId).collect(Collectors.toSet());

    }

    /**
     * Filters rentals based on a predicate
     * @param predicate, the filtering method to apply
     * @return a {@code Set} containing the rentals which respect the given predicate
     */
    public Set<Rental> filterCustom(Predicate<? super Rental> predicate){
        return StreamSupport.stream(rentalRepository.findAll().spliterator(), false).filter(predicate).collect(Collectors.toSet());
    }

    /**
     * Gets a statistics map from every movie to its rentals
     * @return an {@code Map} mapping from every Movie its set of rentals
     */
    public Map<Movie,Set<Rental>> getMovieRental(){

        Iterable<Movie> movies=movieRepository.findAll();
        return StreamSupport.stream(movies.spliterator(),false)
                .collect(Collectors.toMap(m->m,m->getRentalByMovie(m.getId())));
                
    }

    /**
     * Gets a statistics map from every client to its rentals
     * @return an {@code Map} mapping from every Client its set of rentals
     */
    public Map<Client,Set<Rental>> getClientRental(){
        Iterable<Client> clients=clientRepository.findAll();
        return StreamSupport.stream(clients.spliterator(),false)
                .collect(Collectors.toMap(client -> client,
                        client -> getRentalsByClient(client.getId())));
    }


    public Stream<Rental> findAllPaged(){
        if(!(rentalRepository instanceof PagingRepository)){
            throw new MovieRentalException("Cannot use this functionality");
        }
        Page<Rental> rentalPage = ((PagingRepository<Integer, Rental>) rentalRepository).findAll(pageGenerator);
        pageGenerator = rentalPage.getNextPage();
        Optional.of(rentalPage.getElements().count())
                .filter(c->c==0)
                .ifPresent(c->pageGenerator=new PageGenerator());

        return rentalPage.getElements();
    }











    public void initID(){
        ID .addAndGet((int)StreamSupport.stream(rentalRepository.findAll().spliterator(),false).count());
    }
}
