package service;

import domain.BaseEntity;
import domain.Client;
import domain.exceptions.MovieRentalException;
import domain.exceptions.ValidatorException;
import pagination.Page;
import pagination.PageGenerator;
import repository.PagingRepository;
import repository.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ClientService implements Service<Long,Client> {

    private Repository<Long,Client> repository;
    private PageGenerator pageGenerator = new PageGenerator();

    /**
     * Constructor
     * @param repo, the client repository
     */
    public ClientService(Repository<Long,Client> repo){
        repository=repo;
    }

    /**
     * Adds a {@code Client} to the repository
     * @param client, the Client to add to the repository
     * @throws ValidatorException, if the client is not valid
     */
    public Optional<Client> addClient(Client client) throws ValidatorException{
       return repository.save(client);
    }

    /**
     * Deletes a {@code Client} from the repository
     * @param id, representing the identification of a Client
     * @return true if the Client was found and deleted, false otherwise
     * @throws ValidatorException
     */

    public Optional<Client> deleteClient(Long id) throws ValidatorException{
        return repository.delete(id);
    }

    /**
     * @return an {@code Set<Client>} of the entire collection of Clients.
     */
    public Set<Client> getAllClients(){
        return StreamSupport.stream(repository.findAll().spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * filter the clients based on a predice
     * @param predicate a {@code Predicate} predicate which takes in a Client and returns true if the Client matches the conditions
     * @return A set of clients, which match the predicate
     */
    public Set<Client> filterCustom(Predicate<? super Client> predicate){

        return StreamSupport.stream(repository.findAll().spliterator(), false).filter(predicate).collect(Collectors.toSet());
    }

    /**
     * Updates an existing client
     * @param newClient, the client with the new information of the client
     * @return an {@code Optional} which contains the previous movie information
     */
    public Optional<Client> update(Client newClient){
        return repository.update(newClient);
    }

    /**
     * Retrieves a client by its id
     * @param cnp, the clients id
     * @return an {@code Optional} containing the client with the given id or null if it does not exist
     */
    public Optional<Client> findOne(long cnp){
        return repository.findOne(cnp);
    }

    public Stream<Client> findAllPaged(){
        if(!(repository instanceof PagingRepository)){
            throw new MovieRentalException("Cannot use this functionality");
        }
        Page<Client> clientPage = ((PagingRepository<Long, Client>) repository).findAll(pageGenerator);
        pageGenerator = clientPage.getNextPage();
        Optional.of(clientPage.getElements().count())
                .filter(c->c==0)
                .ifPresent(c->pageGenerator=new PageGenerator());

        return clientPage.getElements();
    }
}
