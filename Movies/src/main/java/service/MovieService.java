package service;

import domain.Movie;
import domain.exceptions.MovieRentalException;
import domain.exceptions.ValidatorException;
import pagination.Page;
import pagination.PageGenerator;
import repository.PagingRepository;
import repository.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MovieService implements Service<Integer,Movie> {
    private Repository<Integer, Movie> repo;
    private PageGenerator pageGenerator = new PageGenerator();

    /**
     * Constructor
     * @param repository, the movie repository
     */
    public MovieService(Repository<Integer, Movie> repository){
        repo = repository;
    }

    /**
     * Adds a {@code Movie} to the corresponding repository.
     * @param movie
     *              must not be null/ in a sound state
     * @return a boolean, true if successfully added a movie or false otherwise
     * @throws ValidatorException
     *          if the movie is not in a sound state
     */
    public Optional<Movie> addMovie(Movie movie) throws ValidatorException {
        return repo.save(movie);
    }

    /**
     * Deletes a {@code Movie} from the corresponding repository.
     * @param ID
     *          integer, the ID of the movie to delete
     * @return a boolean, true if successfully deleted the movie or false otherwise.
     * @throws ValidatorException
     *          to comply with the signature of the repository.
     */
    public Optional<Movie> deleteMovie(int ID) throws ValidatorException{
        return repo.delete(ID);
    }

    /**
     *
     * @return an {@code Set<Movie>} of the entire collection of movies.
     */
    public Set<Movie> getAllMovies(){
        return StreamSupport.stream(repo.findAll().spliterator(), false).collect(Collectors.toSet());
    }
    /**
     * filter the movies based on a predice
     * @param predicate a {@code Predicate} predicate which takes in a Movie and returns true if the Client matches the conditions
     * @return A set of movies, which match the predicate
     */
    public Set<Movie> filterCustom(Predicate<? super Movie> predicate){

        return StreamSupport.stream(repo.findAll().spliterator(), false).filter(predicate).collect(Collectors.toSet());
    }

    /**
     * Updates a movie information
     * @param newMovie the movie with the new information
     * @return an {@code Optional} which contains the previous movie information
     */
    public Optional<Movie> update(Movie newMovie){
        return repo.update(newMovie);
    }

    /**
     * Retrieves a movie by its id
     * @param id, the movie id
     * @return an {@code Optional} containing the movie with the given id or null if it does not exist
     */
    public Optional<Movie> findOne(int id){
        return repo.findOne(id);
    }

    public Stream<Movie> findAllPaged(){
        if(!(repo instanceof PagingRepository)){
            throw new MovieRentalException("Cannot use this functionality");
        }
        Page<Movie> moviePage = ((PagingRepository<Integer, Movie>) repo).findAll(pageGenerator);
        pageGenerator = moviePage.getNextPage();
        Optional.of(moviePage.getElements().count())
                .filter(c->c==0)
                .ifPresent(c->pageGenerator=new PageGenerator());

        return moviePage.getElements();
    }
}
