package domain.exceptions;

import domain.exceptions.MovieRentalException;

/**
 * @author rauldarius
 */

public class ValidatorException extends MovieRentalException {

    public ValidatorException(String message) {
        super(message);
    }

}
