package domain.exceptions;

import domain.exceptions.MovieRentalException;

public class ClientNotFound extends MovieRentalException {

    public ClientNotFound(String message) {
        super(message);
    }
}
