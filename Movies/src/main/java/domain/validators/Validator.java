package domain.validators;

import domain.exceptions.ValidatorException;

/**
 * @author rauldarius
 */

public interface Validator<T> {

    public void validate(T entity) throws ValidatorException;
}
