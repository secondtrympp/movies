package domain.validators;
import domain.Client;
import domain.exceptions.ValidatorException;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ClientValidator implements Validator<Client> {
    @Override
    public void validate(Client entity) throws ValidatorException {
         Boolean b = (entity.getId()== null || entity.getId()<0 || entity.getName()==null || !(entity.getName().length()>2) ||
              entity.getEmail()==null  || !entity.getEmail().contains("@") || entity.getAge()<=0);
         Optional<Boolean> booleanOptional= Stream.of(b).filter(v -> !v).findAny();
         booleanOptional.orElseThrow(() -> new ValidatorException("Client details not valid!"));

    }
}
