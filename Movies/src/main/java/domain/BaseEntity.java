package domain;

/**
 * @author rauldarius
 * An abstract class representing the one of the bussiness entities of the domain.
 */

public abstract class BaseEntity<ID> {

    private ID id;

    public ID getId(){
        return id;
    }

    public void setId(ID newId){
        id=newId;
    }

}
