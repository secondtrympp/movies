package domain;

import java.util.Set;
import java.util.function.Predicate;

import service.Service;

/**
 * A custom generic filter which filters entities based on their service and a filter function
 */
public class CustomFilter {

    public static<ID,T extends BaseEntity<ID>> Set<T> customFilter(Service<ID,T> service, Predicate<? super T> predicate){
        return service.filterCustom(predicate);
    }

}
