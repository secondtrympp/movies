package filter;

import domain.Client;
import domain.Movie;
import domain.Rental;
import service.ClientService;
import service.MovieService;
import service.RentalService;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * DEPRECATED
 */
public class Filter {
    private ClientService clientService;
    private MovieService movieService;
    private RentalService rentalService;


    public Filter(ClientService clientService, MovieService movieService, RentalService rentalService) {
        this.clientService = clientService;
        this.movieService = movieService;
        this.rentalService = rentalService;
    }

    /**
     * Filters the movies based on the rating.
     * @param rating a float
     * @return a {@code Set<Movie>} containing the movies which have a rating > the given param.
     */
    public Set<Movie> filterMoviesRating(float rating) {
        return movieService.filterCustom((movie -> movie.getRating() > rating));
    }

    /**
     * Filters the movies based on the genre.
     * @param genre the Genre of the movies
     * @return a {@code Set<Movie>} containing the movies which have the given param as genre.
     */
    public Set<Movie> filterMoviesGenre(String genre) {
        return movieService.filterCustom((movie -> movie.getGenre().equals(genre)));
    }

    /**
     * Filters the clients based on their ages.
     * @param age
     *        the filtered age
     * @param sign
     *        a {@code String} representing either "<", or ">"
     * @return
     *        a {@code Set<Client>} containg the clients which have their age greater or less than the age, based on the sign
     */
    public Set<Client> filterClientsAge(int age, String sign) {
        switch (sign) {
            case "<":
                return clientService.filterCustom(c -> c.getAge() < age);
            case ">":
                return clientService.filterCustom(c -> c.getAge() > age);
            default:
                return new HashSet<>();

        }
    }

    /**
     * Filters the rentals based on their duration.
     * @param duration
     *        the filtered duration
     * @param sign
     *        a {@code String} representing either "<", or ">"
     * @return
     *        a {@code Set<Rental>} containg the rentals which have their duration greater or less than the duration, based on the sign
     */
    public Set<Rental> filterRentalDuration(int duration, String sign) {
        switch (sign) {
            case "<":
                return rentalService.filterCustom(r -> r.getDuration() < duration);
            case ">":
                return rentalService.filterCustom(r -> r.getDuration() > duration);
            default:
                return new HashSet<>();
        }
    }

    /**
     * Filters the rentals based on movie ID.
     * @param mID the ID of the movie
     * @return a {@code Set<Movie>} containing the rentals which have mID as their movie ID
     */
    public Set<Rental> filterRentalMovie(int mID){
        return rentalService.filterCustom(r -> r.getMovieId() == mID);
    }
    /**
     * Filters the rentals based on client ID.
     * @param cID the ID of the client
     * @return a {@code Set<Movie>} containing the rentals which have cID as their client ID
     */
    public Set<Rental> filterRentalClient(long cID){
        return rentalService.filterCustom(r -> r.getClientId() == cID);
    }

}
