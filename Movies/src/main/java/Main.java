import domain.Client;
import domain.Movie;
import domain.Rental;
import domain.validators.ClientValidator;
import domain.validators.MovieValidator;
import domain.validators.RentalValidator;
import repository.*;
/*import repository.util.db.ClientDB;
import repository.util.db.MovieDB;
import repository.util.db.RentalDB;*/
import repository.util.txt.ClientTXT;
import repository.util.txt.MovieTXT;
import repository.util.txt.RentalTXT;
import repository.util.xml.ClientXML;
import repository.util.xml.MovieXML;
import repository.util.xml.RentalXML;
import service.ClientService;
import service.MovieService;
import service.RentalService;
import ui.Console;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDate;


public class Main {


    private static void createEntities(Repository<Integer, Movie> movieRepository, Repository<Long, Client> clientRepository, Repository<Integer, Rental> rentalRepository) {
        movieRepository.save(new Movie(1, "Star Wars", "SF", 4.7f));
        movieRepository.save(new Movie(2, "Django", "Thriller", 3.4f));
        clientRepository.save(new Client(1234L, "Dinu", "dinu@cs.ubb", 22));
        clientRepository.save(new Client(3456L, "Cornel", "cornel@cs.ubb", 35));
        rentalRepository.save(new Rental(19, 1234L, 1, LocalDate.of(2019, 1, 12), LocalDate.now()));
        rentalRepository.save(new Rental(20, 3456L, 2, LocalDate.of(2019, 3, 7), LocalDate.now()));

    }

    public static void main(String[] args) {


        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            boolean read = false;
            while (!read) {
                System.out.print("Enter the file type to load:");
                String cmd = reader.readLine();
                switch (cmd) {
                    case "TXT": {
                        MovieValidator movieValidator = new MovieValidator();
                        Repository<Integer, Movie> movieRepository = new FileRepo<>(movieValidator, "./src/main/resources/movies.txt",
                                MovieTXT::movieFromLine, MovieTXT::fromMovie);
                        MovieService movieService = new MovieService(movieRepository);

                        ClientValidator clientValidator = new ClientValidator();
                        Repository<Long, Client> clientRepository = new FileRepo<>(clientValidator, "./src/main/resources/clients.txt",
                                ClientTXT::clientFromLine, ClientTXT::fromClient);
                        ClientService clientService = new ClientService(clientRepository);

                        RentalValidator rentalValidator = new RentalValidator();
                        Repository<Integer, Rental> rentalRepository = new FileRepo<>(rentalValidator, "./src/main/resources/rentals.txt",
                                RentalTXT::rentalFromLine, RentalTXT::fromRental);
                        RentalService rentalService = new RentalService(rentalRepository, movieRepository, clientRepository);


                        Console console = new Console(movieService, clientService, rentalService);

                        console.run();
                    }
                    read = true;
                    break;
                    case "XML": {
                        MovieValidator movieValidator = new MovieValidator();
                        Repository<Integer, Movie> movieRepository = new XMLRepo<>(movieValidator, "./src/main/resources/movies.xml",
                                MovieXML::movieFromXML, MovieXML::getMovieTag, MovieXML::movieToXML);
                        MovieService movieService = new MovieService(movieRepository);

                        ClientValidator clientValidator = new ClientValidator();
                        Repository<Long, Client> clientRepository = new XMLRepo<>(clientValidator, "./src/main/resources/clients.xml",
                                ClientXML::clientFromXML, ClientXML::getClientTag, ClientXML::clientToXML);
                        ClientService clientService = new ClientService(clientRepository);

                        RentalValidator rentalValidator = new RentalValidator();
                        Repository<Integer, Rental> rentalRepository = new XMLRepo<>(rentalValidator, "./src/main/resources/rentals.xml",
                                RentalXML::rentalFromXML, RentalXML::getRentalTag, RentalXML::rentalToXML);
                        RentalService rentalService = new RentalService(rentalRepository, movieRepository, clientRepository);


                        Console console = new Console(movieService, clientService, rentalService);

                        console.run();
                    }
                    case "DB":{
                        /*MovieValidator movieValidator = new MovieValidator();
                            Repository<Integer, Movie> movieRepository = new DBGenericRepo<>(MovieDB::getMovieSelect, MovieDB::findAll, MovieDB::saveMovie,
                                    MovieDB::updateMovie, MovieDB::findOneMovie, MovieDB::deleteMovie, movieValidator);
                        MovieService movieService = new MovieService(movieRepository);

                        ClientValidator clientValidator = new ClientValidator();
                        Repository<Long, Client> clientRepository = new DBGenericRepo<>(ClientDB::getClientSelect,
                                ClientDB::findAll, ClientDB::saveClient, ClientDB::updateClient,
                                ClientDB::findOneClient, ClientDB::deleteClient, clientValidator);
                        ClientService clientService = new ClientService(clientRepository);

                        RentalValidator rentalValidator = new RentalValidator();
                        Repository<Integer, Rental> rentalRepository = new DBGenericRepo<>(RentalDB::getRentalSelect,
                                RentalDB::findAll, RentalDB::saveRental, RentalDB::updateRental,
                                RentalDB::findOneRental, RentalDB::deleteRental, rentalValidator);
                        RentalService rentalService = new RentalService(rentalRepository, movieRepository, clientRepository);


                        Console console = new Console(movieService, clientService, rentalService);

                        console.run();*/
                        System.out.println("Case DB");
                    }
                    read = true;
                    break;
                    default:
                        System.out.println("Invalid command");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}